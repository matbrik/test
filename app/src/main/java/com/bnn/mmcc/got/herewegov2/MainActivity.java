package com.bnn.mmcc.got.herewegov2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Context mContext;
    private String mToken;
    private String mEmail;
    private String mPassword;
    private UserResponse mUserResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = this;
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sharedpreferences), Context.MODE_PRIVATE);
        mEmail = sharedPreferences.getString(getString(R.string.user_email), "");
        mPassword = sharedPreferences.getString(getString(R.string.user_password), "");


        new UserInformationGetter(mEmail, mPassword).execute();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void Update() {

        if (mUserResponse == null)
            return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView1 = (TextView) findViewById(R.id.nav_user_name);
                TextView textView2 = (TextView) findViewById(R.id.nav_user_email);
                textView1.setText(mUserResponse.getData().getName());
                textView2.setText(mUserResponse.getData().getEmail());

                ImageView imageView = (ImageView) findViewById(R.id.imageView);

                try {
                    URL url = new URL(mUserResponse.getData().getProfileImage());
                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    imageView.setImageBitmap(bmp);
                } catch (MalformedURLException e) {
                    return;
                } catch (IOException e) {
                    return;
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class UserInformationGetter extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserInformationGetter(String email, String password) {
            mEmail = email;
            mPassword = password;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {
                HttpClient httpClient = new DefaultHttpClient();
                LoginRequest pojoRequest = new LoginRequest();
                pojoRequest.setPassword(mPassword);
                pojoRequest.setEmail(mEmail);
                String postUrl = "http://www.mycarh2polito.tk/api/login";// put in your url
                Gson gson = new Gson();
                HttpPost post = new HttpPost(postUrl);
                StringEntity postingString = null;//convert your pojo to   json
                postingString = new StringEntity(gson.toJson(pojoRequest));

                post.setEntity(postingString);
                post.setHeader("Content-type", "application/json");
                HttpResponse response = httpClient.execute(post);
                if (response == null) {
                    Intent loginIntent = new Intent(mContext, LoginActivity.class);
                    startActivity(loginIntent);
                }
                InputStream in = response.getEntity().getContent(); //Get the data in the entity
                final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                LoginResponse loginResponse = gson.fromJson(reader, LoginResponse.class);
                if (loginResponse.getToken() == null) {
                    Intent loginIntent = new Intent(mContext, LoginActivity.class);
                    startActivity(loginIntent);
                    return false;
                }
                mToken = loginResponse.getToken();


                String getUrl = "http://www.mycarh2polito.tk/api/profile";
                HttpGet httpGet = new HttpGet(getUrl);
                httpGet.setHeader("Authorization", "Bearer " + mToken);
                response = httpClient.execute(httpGet);
                if (response == null) {
                    Intent loginIntent = new Intent(mContext, LoginActivity.class);
                    startActivity(loginIntent);
                }
                in = response.getEntity().getContent(); //Get the data in the entity
                final BufferedReader reader2 = new BufferedReader(new InputStreamReader(in));
                UserResponse userResponse = gson.fromJson(reader2, UserResponse.class);
                if (!userResponse.isFlag()) {
                    Intent loginIntent = new Intent(mContext, LoginActivity.class);
                    startActivity(loginIntent);
                    return false;
                }
                mUserResponse = userResponse;
            } catch (UnsupportedEncodingException e) {
                return false;
            } catch (IOException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            Update();

        }

        @Override
        protected void onCancelled() {

        }
    }


}
